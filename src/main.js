import Vue from 'vue'
import Vuesax from 'vuesax'
import App from './App.vue'

import store from './store/store'

Vue.use(Vuesax);

Vue.config.productionTip = false

new Vue({
  store:store,
  render: h => h(App),
}).$mount('#app')
