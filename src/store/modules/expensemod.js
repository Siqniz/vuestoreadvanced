import {paymentFrequencyEnum} from '../../util/util'

const state = {
    expenses: [
        {
            'name': 'Rent',
            'cost': 1147.73,
            'created-date': '',
            'frequecy': paymentFrequencyEnum['MONTHLY']
        },
        {
            'name': 'Phone',
            'cost': 150,
            'created-date': '',
            'frequecy':paymentFrequencyEnum['MONTHLY']
        },
        {
            'name':'Storage Unit',
            'cost': 150,
            'created-date': '',
            'frequecy': paymentFrequencyEnum['MONTHLY']
        },
        {
            'name':'Insurance',
            'cost': 147,
            'created-date': '',
            'frequecy': paymentFrequencyEnum['MONTHLY']
        },
        {
            'name':'Gas',
            'cost': 30,
            'created-date': '',
            'frequecy': paymentFrequencyEnum['BI_MONTHLY']
        },
        {
            'name':'Mailbox',
            'cost': 60,
            'created-date': '',
            'frequecy': paymentFrequencyEnum['TRIMESTER']
        }
    ],
    expense: {},
}

const getters = {
    getExpenses: state => {
        return state.expenses
    },
    getExpense: state => {
        return state.expense;
    },
    totalCost: (state) => {
        return state.expenses.reduce((cumu, ele) => {
            return cumu += (ele.cost * ele.frequecy);
        }, 0.00);
    }
}

const mutations = {
    updateExpenses: (state, payload) => {
        state.expenses.concat(payload)
    },
    updateExpense: (state, payload) => {
        state.expense = payload;
    },
}

export default {
    state,
    getters,
    mutations
}