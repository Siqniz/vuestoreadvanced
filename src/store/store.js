import Vue from 'vue';
import Vuex from 'vuex'
import expensemod from './modules/expensemod'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules:[
        expensemod
    ]
});

export default store;